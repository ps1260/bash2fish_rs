# bash2fish (Rust Version)

This is a reimplementation of my [bash2fish](https://gitlab.com/ps1260/bash2fish) utility, which was written in Go. It is 
not identical in operation, but should already have most of the same functionality. 
If the Go version worked for you, so should this.

To build, 'cd bash2fish_rs' and 'cargo build --release', or if you do not have cargo 
installed, you may move 'cd bash2fish_rs/src' and 'rustc bash2fish.rs'.
