#[cfg(test)]
mod test {
    use convert_line;

    #[test]
    fn it_works() {
        let mut f = false;
        let mut t = true;
        assert_eq!(
            convert_line(&"alias foo=bar".to_string(), &mut f),
            "alias foo=bar"
        );
        assert_eq!(
            convert_line(&"alias foo='bar&&baz'".to_string(), &mut f),
            "alias foo='bar; and baz'"
        );
        assert_eq!(
            convert_line(&"alias foo='bar||baz'".to_string(), &mut f),
            "alias foo='bar; or baz'"
        );
        assert_eq!(
            convert_line(&"alias foo=' !bar||baz'".to_string(), &mut f),
            "alias foo=' not bar; or baz'"
        );
        assert_eq!(
            convert_line(&"export foo=bar".to_string(), &mut f),
            "set --export foo bar"
        );
        assert_eq!(
            convert_line(&"export foo='bar:baz'".to_string(), &mut f),
            "set --export foo 'bar baz'"
        );
        assert_eq!(
            convert_line(&"unset foo".to_string(), &mut f),
            "set --erase foo"
        );

        assert_eq!(
            convert_line(&"function foo() {".to_string(), &mut t),
            "function foo "
        );
        assert_eq!(convert_line(&"bar $@".to_string(), &mut t), "bar $argv");
        assert_eq!(convert_line(&"}".to_string(), &mut t), "end");

        assert_eq!(
            convert_line(&"function foo() {".to_string(), &mut t),
            "function foo "
        );
        assert_eq!(
            convert_line(&"bar $#".to_string(), &mut t),
            "bar (count $argv)"
        );
        assert_eq!(convert_line(&"}".to_string(), &mut t), "end");
    }
}
