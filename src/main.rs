use std::string::String;
extern crate pico_args;
use std::fs::read_to_string;
use std::fs::File;
use std::io::prelude::*;

mod test;

struct Args {
    help: bool,
    sudofix: bool,
    version: bool,
    free: Vec<std::ffi::OsString>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let helpstring = format!(
        "{} - {}

USAGE:
    bash2fish [FLAGS] <file>

FLAGS:
    -h, --help       Prints help information
    -s, --sudofix    Add alias to enable 'sudo !!'
    -V, --version    Prints version information

ARGS:
    <file>",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );
    let mut args = pico_args::Arguments::from_env();
    // Arguments can be parsed in any order.
    let args = Args {
        help: args.contains(["-h", "--help"]),
        sudofix: args.contains(["-s", "--sudofix"]),
        version: args.contains(["-v", "--version"]),
        // Will return all free arguments or an error if any flags are left.
        free: args.finish(),
    };
    if args.version {
        println!("{} - {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
        return Ok(());
    }
    if args.help || args.free.len() != 1 {
        println!("{}", helpstring);
        return Ok(());
    }

    let file = &args.free[0];

    //let bash_file_content = read_file(file)?;
    let bash_file_content = read_to_string(file)?;
    let bash_lines = bash_file_content.lines();
    let mut is_function = false;

    //Convert each of the lines
    let mut fish_lines: Vec<String> = bash_lines
        .map(|line: &str| convert_line(line, &mut is_function))
        .collect();

    //Add the optional sudofix
    if args.sudofix {
        fish_lines.push("function sudo".to_string());
        fish_lines.push("\tif test \"$argv\" = !!".to_string());
        fish_lines.push("\t\teval command sudo $history[1]".to_string());
        fish_lines.push("\telse".to_string());
        fish_lines.push("\t\tcommand sudo $argv".to_string());
        fish_lines.push("\tend".to_string());
        fish_lines.push("end".to_string());
    }

    //Convert vector of strings into a single string
    let fish_file_content = fish_lines.join("\n");

    //Write the converted configurations into a config.fish file
    write_to_file("config.fish", &fish_file_content)?;
    Ok(())
}

fn write_to_file(path: &str, data: &str) -> std::io::Result<()> {
    let mut f = File::create(path)?;
    f.write_all(data.as_bytes())?;
    f.sync_all()?;
    Ok(())
}

//Takes a line of bash config, and converts to a line of fish
//is_function tells whether the given line is within a function
fn convert_line(line: &str, is_function: &mut bool) -> String {
    if !line.is_empty() && !line.starts_with('#') {
        let mut newline = line
            .replace("&&", "; and ")
            .replace("||", "; or ")
            .replace(" !", " not ")
            .replace("unset", "set --erase")
            .replace("$@", "$argv")
            .replace("(", "")
            .replace(")", "")
            .replace("$#", "(count $argv)");
        if newline.contains("export") {
            newline = newline
                .replace("export", "set --export")
                .replace("=", " ")
                .replace(":", " ")
                .replace("\"", " ");
        }
        //Reformat functions. Fish does not use brackets, but rather opens
        //and closes the function with "function" and "end"
        if *is_function && newline.contains('}') {
            newline = newline.replace("}", "end");
            *is_function = false;
        } else if newline.contains("function") {
            newline = newline.replace("{", "");
            *is_function = true;
        }
        newline
    } else {
        line.to_string()
    }
}
